var fs     = require("graceful-fs");
var util   = require("util");
var exec   = require('child_process').exec;
var wrench = require("wrench");


module.exports = function(dir, fn){
	
	var patterns = [
		'gzinflate(base64_decode',
		'eval(gzinflate(base64_decode',
		'(new Function(String.fromCharCode(',
		'eval(base64_decode',
		'eval(',
		'str_rot13',
		'FilesMan',
		'Adminer',
		'ZXZhbCg',
		'YmFzZTY0X2RlY29kZ',
		'\\x47\\x4c\\x4f\\x42\\x41\\x4c\\x53',
		'\\x65\\x76\\x61\\x6C\\x28',
		'passthru',
		'shell_exec',
		'gzpassthru',
		'\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28',
		'$_F=__FILE__;$_X='
	];


	function FindMalware(kwargs, fn){
		this.path     = kwargs.directory;
		this.patterns = kwargs.patterns;
		this.timex    = new Date();
		this.files    = wrench.readdirSyncRecursive(this.path);
		this.results  = [];
	}
	FindMalware.prototype = {
		start: function(fn){
			console.log('Script ready...');
		
			console.log('Getting files...');

			this.work(function(result){
				fn(result);
			});
			console.info( 'Done! \nOperation take %ds', (((new Date() - this.timex)/1000)%60));
			
		},
		getLines: function(fn){
			var files = this.files;
			var line = 0;
			var data = [];

			for( var i = 0; i < files.length; i++ ){
				var location = util.format('%s%s', dir, files[i]);
				if( !fs.lstatSync(location).isDirectory() 
				&& location.indexOf('.php') != -1 ){	
					var stream = new wrench.LineReader(dir+files[i]);

					if( !stream.hasNextLine() )	line = 0;

					do {
						this.check(stream.getNextLine(), files[i], line, function(found){
							data.push(found);
						});
						line++;
					} while( stream.hasNextLine() );

					if( i == files.length-1 ) fn(data);
				}		
			}
		},
		work: function(fn){
			this.getLines(function(result){
				fn(result);
			});
		},
		check: function(line, file, lineNumber, fn){
			var data = [];
		
			for( var i = 0; i< this.patterns.length; i++ ){
				
				var pattern         = this.patterns[i];
				var scapedRegex     = this.scapeRegex( pattern );
				var problem         = line.toString().substring(0,100).trim();
				var buildRegex      = new RegExp( '(' + scapedRegex + '(.*))', 'gi' );
				var isBeginAComment = problem.substring(0, 2)  == '//' ? true : false;
				

				if( line.indexOf( pattern ) !== -1 
					&& line.match( buildRegex ) ){
					data = { 
						state: "found", 
						file: file, 
						line: lineNumber, 
						fileInfo: fs.lstatSync(dir+ file),
						chunk: problem, 
						pattern: pattern,
						seensDangerous: isBeginAComment ? false : true
					};

					fn(data);
					return;
				}
			}
		},
		scapeRegex: function(str){
	  		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
		},
		execCmd: function(cmd, fn){			
			var command = exec(cmd);
			var result  = "";

			command.stdout.on('data', function(data){
				fn( data );
			});
		}
	};
	var finder = new FindMalware({ 
		directory: dir,
		patterns: patterns
	});	

	finder.start(function(res){
		fn(res);
	});
}
